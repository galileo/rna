# rna

exploitation du Registre National des Associations (RNA)


La source des données se trouve à cette adresse

https://www.data.gouv.fr/fr/datasets/repertoire-national-des-associations/

## prérequis

```bash
sudo apt install wget
sudo apt install unzip
sudo apt install csvtool
```

## téléchargement des fichiers

```bash
wget https://www.data.gouv.fr/fr/datasets/r/8c3384ab-bd29-43df-9730-8c89a88e634a
wget https://www.data.gouv.fr/fr/datasets/r/9653e936-c7fc-4d6b-9458-c46869f8ebef
```

## renomage

```bash
mv 8c3384ab-bd29-43df-9730-8c89a88e634a import.zip
mv 9653e936-c7fc-4d6b-9458-c46869f8ebef waldec.zip
```

- RNA_waldec : Toutes les associations créées ou ayant déclaré un changement de situation depuis 2009

- RNA_import : pas de déclaration de changement de situation depuis 2009.

## extraction des données départementales contenues dans les fichiers zip précédement téléchargées
Liste des données concernants la Sarthe dans le fichier zip waldec
```bash
unzip -l waldec.zip |grep 72
```
Résultat
```bash
...
10998779  2020-09-01 04:31   rna_waldec_20200901_dpt_72.csv
...
```
Extraction des donnés pour la Sarthe du fichier waldec
```bash
unzip  waldec.zip rna_waldec_20200901_dpt_72.csv
```
Liste des données concernants la Sarthe dans le fichier zip import
```bash
unzip -l import.zip |grep 72
```
Résultat
```bash
...
3606048  2020-09-01 04:35   rna_import_20200901_dpt_72.csv
...
```
Extraction des données concernants la Sarthe dans le fichier zip import
```bash
unzip  import.zip  rna_import_20200901_dpt_72.csv
```
Décompte des lignes de chaque fichier pour information
```bash
csvtool -t ";"  height  rna_waldec_20200901_dpt_72.csv 
18842
csvtool -t ";"  height  rna_import_20200901_dpt_72.csv
9156
```
En Sarthe 
- 18 842 associations ont déclaré un changement depuis 2009 
-  9 156 associations n'ont déclaré aucun changement


## Comparaison des colonnes

```bash
csvtool -t ";"  head 1 rna_import_20200901_dpt_72.csv | tr "," "\n" > import.col
csvtool -t ";"  head 1 rna_waldec_20200901_dpt_72.csv | tr "," "\n" > waldec.col

diff -y import.col  waldec.col 

id								id
id_ex								id_ex
siret								siret
							      >	rup_mi
gestion								gestion
date_creat							date_creat
							      >	date_decla
date_publi							date_publi
							      >	date_disso
nature								nature
groupement							groupement
titre								titre
							      >	titre_court
objet								objet
objet_social1							objet_social1
objet_social2							objet_social2
adr1							      |	adrs_complement
adr2							      |	adrs_numvoie
adr3							      |	adrs_repetition
adrs_codepostal						      |	adrs_typevoie
libcom							      |	adrs_libvoie
							      >	adrs_distrib
adrs_codeinsee							adrs_codeinsee
							      >	adrs_codepostal
							      >	adrs_libcommune
							      >	adrg_declarant
							      >	adrg_complemid
							      >	adrg_complemgeo
							      >	adrg_libvoie
							      >	adrg_distrib
							      >	adrg_codepostal
							      >	adrg_achemine
							      >	adrg_pays
dir_civilite							dir_civilite
siteweb								siteweb
							      >	publiweb
observation							observation
position							position
rup_mi							      <
maj_time							maj_time
```

On constate que les entête ne sont pas identiques il faudra faire un traitement différent pour les deux fichiers ou se limiter aux colonnes présentes dans les deux fichiers

## Convertion en UTF8

afin de permettre des recherches sur les caratères accentués, il faut convertir les fichiers en UTF8

```bash
iconv -f windows-1252 -t utf-8 rna_waldec_20200901_dpt_72.csv > waldecUTF8.csv
iconv -f windows-1252 -t utf-8 rna_import_20200901_dpt_72.csv > importUTF8.csv
```

## Liste des associations dont l'objet contient «solidarité internationale»

```bash
csvtool  -t ";" namedcol titre,adrg_libvoie,adrg_codepostal,adrg_achemine,adrg_pays,objet  waldecUTF8.csv |egrep  -i "solidarité internationale"|csvtool col 1,2,3,4,5 -

...
CENTRE FRANCAIS DE SECOURISME DE LA SARTHE (CFS 72),9  rue Saint Louis,72510,Pontvallain,FRANCE
TCHEL'KAN,,72190,NEUVILLE SR SARTHE,FRANCE
D'TOURS DURABLES,11 rue des Perrons,72000,Le    Mans,FRANCE
...
```
